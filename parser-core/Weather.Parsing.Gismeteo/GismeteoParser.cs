﻿using AngleSharp;
using AngleSharp.Dom;
using AngleSharp.Html.Parser;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Weather.Data;

namespace Weather.Parsing.Gismeteo
{
    /// <summary>
    /// GismeteoTomorrowParser get weather data from gismetio.ru site for tomorrow.
    /// Class uses AngleSharp for site parsing.
    /// </summary>
    public class GismeteoTomorrowParser : IParser
    {
        /// <summary>
        /// Create new object
        /// </summary>
        /// <param name="htmlParser">Htmp parser instance</param>
        /// <param name="context">Browser context instance</param>
        public GismeteoTomorrowParser(HtmlParser htmlParser, IBrowsingContext context)
        {
            this.parser = htmlParser;
            this.context = context;

            var fmt = new NumberFormatInfo();
            fmt.NegativeSign = "-";
            fmt.PositiveSign = "+";

            this.fmtProvider = fmt;
        }

        /// <summary>
        /// Get current data from site
        /// </summary>
        /// <returns></returns>
        public IEnumerable<CityData> Parse()
        {
            var mainPage = context.OpenAsync(address).Result;
            var cities = getCitiesPairs(mainPage).ToArray();
            
            Console.WriteLine("GismeteoParser: cities{{{0}}}", cities.Aggregate("", (item, turple) => $"{item}, {turple.Name}"));

            var dataDate = DateTime.Now.Date.AddDays(1);

            foreach (var cityTurple in cities)
            {
                TemperatureData temperatureData;
                try
                {
                    temperatureData = getTemperatureForCity(cityTurple.HRef);
                    Console.WriteLine($"GismeteoParser: ref: {cityTurple.HRef}, temperatureData{{{temperatureData.Min}, {temperatureData.Max}}}");
                }
                catch (AggregateException e)
                {
                    Console.WriteLine($"GismeteoParser: Error. {e.Message}");
                    continue;
                }

                yield return new CityData(cityTurple.Name, temperatureData, dataDate);
            }
        }

        TemperatureData getTemperatureForCity(string cityRef)
        {
            var cityPage = context.OpenAsync(address + cityRef).Result;

            var values = cityPage
                .QuerySelectorAll(temperatureSelector)
                .Select(item => item.TextContent)
                .ToArray();

            if (TEMPERATURE_LEN != values.Length)
                throw new AggregateException(
                        string.Format("unexpected temperature values count, expected: {0}, got: {1}", TEMPERATURE_LEN, values.Length)
                    );

            int minTemp = int.Parse(values[TEMPERATURE_MIN_INDEX].Replace("−", "-"), fmtProvider), 
                maxTemp = int.Parse(values[TEMPERATURE_MAX_INDEX], fmtProvider);

            cityPage.Close();
            return new TemperatureData(minTemp, maxTemp);
        }

        IEnumerable<(string Name, string HRef)> getCitiesPairs(IDocument page)
        {
            var popularCitiesCode = getPopularCitiesCode(page);
            var popularCities = parsePopularCitiesRef(popularCitiesCode);

            return page.QuerySelectorAll(citiesSelector)
                .Select(
                    item => (item.QuerySelector(citiesNameSelector).TextContent.Trim(), item.GetAttribute("href"))
                )
                .Union(popularCities);
        }

        IEnumerable<(string, string)> parsePopularCitiesRef(string code)
        {
            var dom = parser.ParseDocumentAsync(code).Result;
            
            return dom.QuerySelectorAll(popolarCitiesSelector)
                .Select(
                    item => (item.GetAttribute("data-name"), item.GetAttribute("href"))
                );
        }

        string getPopularCitiesCode(IDocument page) => page.QuerySelector(popolarCitiesCodeSelector)?.InnerHtml;

        HtmlParser parser;
        IBrowsingContext context;


        static readonly string address = "https://www.gismeteo.ru/";
        static readonly string popolarCitiesCodeSelector = "div.column-wrap noscript#noscript";
        static readonly string popolarCitiesSelector = "a";
        static readonly string citiesSelector = "a.cities_link";
        static readonly string citiesNameSelector = "span.cities_name";

        static readonly string temperatureSelector = "div.tabtemp_1line_inner span.unit_temperature_c";

        const int TEMPERATURE_MIN_INDEX = 0;
        const int TEMPERATURE_MAX_INDEX = 1;
        const int TEMPERATURE_LEN = 2;

        IFormatProvider fmtProvider;
    }
}