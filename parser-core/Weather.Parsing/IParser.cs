﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Weather.Data;

namespace Weather.Parsing
{
    /// <summary>
    /// Describe weather site parser interface
    /// </summary>
    public interface IParser 
    {
        IEnumerable<CityData> Parse();
    }

    /// <summary>
    /// Describe weather site async parser interface
    /// </summary>
    public interface IAcyncParser 
    {
        Task<IEnumerable<CityData>> ParseAsync();
    }
}