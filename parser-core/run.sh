#!/bin/bash

dotnet run . --connection-string="server=${WEATHER_DB_ADDR};user=${WEATHER_DB_USER};database=${WEATHER_DB_DATABASE};port=${WEATHER_DB_PORT};password=${WEATHER_DB_PWD}" --update-interval=${WEATHER_UPD_INTERVAL}
#echo "server=${WEATHER_DB_ADDR};user=${WEATHER_DB_USER};database=${WEATHER_DB_DATABASE};port=${WEATHER_DB_PORT};password=${WEATHER_DB_PWD}" --update-interval=${WEATHER_UPD_INTERVAL}
