﻿using System;
using System.Collections.Generic;
using System.Text;
using Weather.Data;

namespace Weather
{

    class Storage : IDisposable
    {
        public Storage(System.Data.IDbConnection connection)
        {
            insert = connection.CreateCommand();
            commandTextBuilder = new StringBuilder(insertValuesCountDefault);
        }

        public void Store(IEnumerable<CityData> data)
        {
            commandTextBuilder.Clear();
            commandTextBuilder.Append(insertTemplate);

            bool isFirst = true;

            foreach (var item in data)
            {
                if (!isFirst)
                    commandTextBuilder.Append(",");
                else
                    isFirst = false;

                var line = $"('{item.Name}',{item.Temperature.Min},{item.Temperature.Max},'{item.OnDate.ToString("yyyy-MM-dd")}')";

                Console.WriteLine($"Storager: append {line}");
                commandTextBuilder.Append(line);
            }

            insert.CommandText = commandTextBuilder.ToString();
            Console.WriteLine("Storager: sql command: {0}", insert.CommandText);
            insert.ExecuteNonQuery();
        }

        public void Dispose()
        {
            if (null != insert)
                insert.Dispose();
        }

        ~Storage()
        {
            Dispose();
        }

        System.Data.IDbCommand insert;
        StringBuilder commandTextBuilder;

        const string insertTemplate = @"
INSERT INTO 
    weather (city_name, temperature_min, temperature_max, on_date) 
VALUES
";
        const int insertValuesCountDefault = 2000;
    }
}