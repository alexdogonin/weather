﻿using System;
using System.ServiceModel;
using System.ServiceModel.Description;

namespace Weather.Service
{
    /// <summary>
    /// Weather service host
    /// </summary>
    class Host
    {
        static void Main(string[] args)
        {
            var listeningAddr = Properties.Settings.Default.ListeningAddr;
            Uri baseAddress = new Uri(listeningAddr);

            ServiceHost selfHost = null;

            try
            {
                selfHost = new ServiceHost(typeof(WeatherService), baseAddress);
                var serviceEndpoint = selfHost.AddServiceEndpoint(typeof(IWeather), new WebHttpBinding(), "WeatherService");
                serviceEndpoint.EndpointBehaviors.Add(new WebHttpBehavior());

                ServiceMetadataBehavior smb = new ServiceMetadataBehavior();
                smb.HttpGetEnabled = true;
                smb.HttpGetUrl = new Uri(baseAddress, "WeatherService/met");
                selfHost.Description.Behaviors.Add(smb);

                selfHost.Open();
                Console.WriteLine("The service is ready.");

                Console.WriteLine("Press <Enter> to terminate the service.");
                Console.WriteLine();
                Console.ReadLine();
                selfHost.Close();
            }
            catch (CommunicationException ce)
            {
                Console.WriteLine("An exception occurred: {0}", ce.Message);
                selfHost?.Abort();
            }

        }
    }
}
