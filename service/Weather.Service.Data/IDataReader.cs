﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Weather.Service.Data
{
    /// <summary>
    /// Interface of data reader
    /// </summary>
    public interface IDataReader
    {
        IEnumerable<CityData> GetData();
    }
}
