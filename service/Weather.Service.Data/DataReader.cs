﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace Weather.Service.Data
{
    /// <summary>
    /// DataReader get data from DB
    /// </summary>
    public class DataReader : IDataReader
    {
        /// <summary>
        /// Create new object
        /// </summary>
        /// <param name="connection">Connection to data base</param>
        public DataReader(System.Data.IDbConnection connection)
        {
            select = connection.CreateCommand();
            select.CommandText = @"
SELECT city_name, temperature_min, temperature_max, on_date 
FROM weather
WHERE 
	on_date = @on_date
    AND created = (
		select max(created)
        from weather as weather_created
        where weather_created.city_name = weather.city_name
    )";

            var parameter = select.CreateParameter();
            parameter.ParameterName = "on_date";
            select.Parameters.Add(parameter);
        }

        /// <summary>
        /// Get data from DB
        /// </summary>
        /// <returns></returns>
        public IEnumerable<CityData> GetData()
        {
            var onDateParm = select.Parameters?["on_date"] as IDbDataParameter;
            if (null == onDateParm) throw new ArgumentNullException("not exist \"on_date\" parameter for \"select\" command");

            onDateParm.Value = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd");

            using (var reader = select.ExecuteReader())
            {
                while(reader.Read())
                {
                    yield return new CityData()
                    {
                        Name = reader.GetString(0),
                        Temperature = new TemperatureData()
                        {
                            Min = reader.GetInt32(1),
                            Max = reader.GetInt32(2)
                        },
                        OnDate = reader.GetDateTime(3)
                    };
                }
            }                
            
        }

        IDbCommand select;
    }
}
