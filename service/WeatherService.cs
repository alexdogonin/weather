﻿using MySql.Data.MySqlClient;
using System.Collections.Generic;
using Weather.Service.Data;

namespace Weather.Service
{
    /// <summary>
    /// WeatherService provides functionality for getting weather data from DB
    /// </summary>
    public class WeatherService : IWeather
    {
        /// <summary>
        /// Create new object
        /// </summary>
        public WeatherService()
        {
            var connection = new MySqlConnection(Properties.Settings.Default.MySQLConnectionString);
            connection.Open();
            this.reader = new DataReader(connection);
        }

        /// <summary>
        /// Get weather data for tomorrow
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Data.CityData> GetTomorrowData() => reader.GetData();

        IDataReader reader;
    }
}
