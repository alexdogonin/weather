﻿using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Timers;
using System.Windows;
using Weather.Service.Client;

namespace Client
{
    /// <summary>
    /// Application main window
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            client = createClient();

            var updIntervalMs = Properties.Settings.Default.UpdateIntervalMs;

            timer = new System.Timers.Timer(updIntervalMs);
            timer.AutoReset = true;
            timer.Elapsed += Timer_Elaped;
            timer.Start();
        }

        ~MainWindow()
        {
            timer?.Dispose();
        }

        private void Timer_Elaped(object sender, System.Timers.ElapsedEventArgs e)
        {
            weatherListBox.Dispatcher.BeginInvoke(
                    new MethodDelegate((list) => weatherListBox.ItemsSource = list),
                    client.GetTomorrowData()
                        .Select(item => $"{item.Name} {item.Temperature.Min}/{item.Temperature.Max}")
                );
        }

        private WeatherClient createClient()
        {
            var binding = new System.ServiceModel.WebHttpBinding();
            var serviceAddr = Client.Properties.Settings.Default.ServiceAddr;

            var client = new WeatherClient(binding, new EndpointAddress(serviceAddr));
            client.Endpoint.EndpointBehaviors.Add(new System.ServiceModel.Description.WebHttpBehavior());

            return client;
        }

        Timer timer;
        WeatherClient client;
        delegate void MethodDelegate(IEnumerable<string> list);
        delegate void InvokeDelegate();
    }
}
