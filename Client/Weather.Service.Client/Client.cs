﻿using System.Collections.Generic;

namespace Weather.Service.Client
{
    /// <summary>
    /// Weather client get data from weather service
    /// </summary>
    class WeatherClient: System.ServiceModel.ClientBase<IWeather>
    {
        public WeatherClient()
        {
        }

        public WeatherClient(string endpointConfigurationName)
            :
                base(endpointConfigurationName)
        {
        }

        public WeatherClient(string endpointConfigurationName, string remoteAddress)
            :
                base(endpointConfigurationName, remoteAddress)
        {
        }

        public WeatherClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress)
            :
                base(endpointConfigurationName, remoteAddress)
        {
        }

        public WeatherClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress)
            :
                base(binding, remoteAddress)
        {
        }

        /// <summary>
        /// Get data for tomorrow
        /// </summary>
        /// <returns>
        /// Collection of city data
        /// </returns>
        public IEnumerable<Weather.Service.Data.CityData> GetTomorrowData()
        {
            return base.Channel.GetTomorrowData();
        }

    }
}
