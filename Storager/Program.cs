﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Sql;
using System.Data.OleDb;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.Timers;
using Weather.Parsing;
using Weather.Parsing.Gismeteo;
using AngleSharp;
using AngleSharp.Html.Parser;

namespace Weather
{
    class Program
    {
        static void Main(string[] args)
        {
            var connection = new MySqlConnection(Properties.Settings.Default.MySQLConnectionString);
            connection.Open();

            var storage = new Weather.Storage(connection);

            var config = Configuration.Default.WithDefaultLoader();
            var context = BrowsingContext.New(config);
            var htmlParser = new HtmlParser();
            var parser = new GismeteoTomorrowParser(htmlParser, context);

            var timer = new Timer(Properties.Settings.Default.UpdatePeriodMin * MinutesToMsCoef);
            timer.AutoReset = true;
            timer.Elapsed += (sender, e) =>
            {
                lock (storage)
                {
                    Console.WriteLine("Geting data.");
                    var data = parser.Parse();

                    Console.WriteLine("Data was recieved.");
                    storage.Store(data);
                    Console.WriteLine("Data was stored.");
                }
            };
                
            timer.Start();
            

            Console.WriteLine("Press <Enter> to terminate the program.");
            Console.WriteLine();
            Console.ReadLine();   
        }

        const int MinutesToMsCoef = 60 * 1000;
    }


}


