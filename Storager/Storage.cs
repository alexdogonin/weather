﻿using System;
using System.Collections.Generic;
using System.Text;
using Weather.Data;

namespace Weather
{

    class Storage : IDisposable
    {
        public Storage(System.Data.IDbConnection connection)
        {
            insert = connection.CreateCommand();
            commandTextBuilder = new StringBuilder(insertValuesCountDefault);
        }

        public void Store(IEnumerable<CityData> data)
        {
            commandTextBuilder.Clear();
            commandTextBuilder.Append(insertTemplate);

            bool isFirst = true;

            foreach (var item in data)
            {
                if (!isFirst)
                    commandTextBuilder.Append(",");
                else
                    isFirst = false;

                commandTextBuilder.Append($"('{item.Name}',{item.Temperature.Min},{item.Temperature.Max},'{item.OnDate.ToString("yyyy-MM-dd")}')");
            }

            insert.CommandText = commandTextBuilder.ToString();
            insert.ExecuteNonQuery();
        }

        public void Dispose()
        {
            if (null != insert)
                insert.Dispose();
        }

        ~Storage()
        {
            Dispose();
        }

        System.Data.IDbCommand insert;
        StringBuilder commandTextBuilder;

        const string insertTemplate = @"
INSERT INTO 
    weather (city_name, temperature_min, temperature_max, on_date) 
VALUES
";
        const int insertValuesCountDefault = 2000;
    }
}