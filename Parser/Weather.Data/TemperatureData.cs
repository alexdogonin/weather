﻿namespace Weather.Data
{
    /// <summary>
    /// Represent pair of minimal and maximal temperature
    /// </summary>
    public struct TemperatureData
    {
        /// <summary>
        /// Create new object
        /// </summary>
        /// <param name="min">Minimal value</param>
        /// <param name="max">Maximal value</param>
        public TemperatureData(int min, int max)
        {
            this.Min = min;
            this.Max = max;
        }

        /// <summary>
        /// Minimal value
        /// </summary>
        public int Min { get; }

        /// <summary>
        /// Maximal value
        /// </summary>
        public int Max { get; }
    }
}