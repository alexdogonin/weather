﻿using System;

namespace Weather.Data
{
    /// <summary>
    /// Represent information about temperature in city on date
    /// </summary>
    public struct CityData
    {
        /// <summary>
        /// Create new object
        /// </summary>
        /// <param name="name">Name of city</param>
        /// <param name="minTemperature">Minimal temperature</param>
        /// <param name="maxTemperature">Maximal temperature</param>
        /// <param name="onDate">Date for which temperature data is valid</param>
        public CityData(string name, int minTemperature, int maxTemperature, DateTime onDate)
        {
            this.Name = name;
            this.Temperature = new TemperatureData(minTemperature, maxTemperature);
            this.OnDate = onDate;
        }

        /// <summary>
        /// Create new object
        /// </summary>
        /// <param name="name">Name of city</param>
        /// <param name="temperatureData">Temperature data</param>
        /// <param name="onDate">Date for which temperature data is valid</param>
        public CityData(string name, TemperatureData temperatureData, DateTime onDate)
        {
            this.Name = name;
            this.Temperature = temperatureData;
            this.OnDate = onDate;
        }

        /// <summary>
        /// City name
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// Temperature data
        /// </summary>
        public TemperatureData Temperature { get; }

        /// <summary>
        /// Date for which temperature data is valid
        /// </summary>
        public DateTime OnDate { get; }
    }
}