﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;


namespace Weather.Service
{
    /// <summary>
    /// IWeather describes weather service contract
    /// </summary>
    [ServiceContract]
    public interface IWeather
    {
        /// <summary>
        /// Describes get data for tomorrow contract
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebGet]
        IEnumerable<Data.CityData> GetTomorrowData();
    }
}
