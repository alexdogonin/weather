﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// Общие сведения о сборке можно задать с помощью следующего 
// атрибутов. Отредактируйте значения этих атрибутов, чтобы изменить
// связанные со сборкой.
[assembly: AssemblyTitle("ServiceLib")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("ServiceLib")]
[assembly: AssemblyCopyright("Copyright ©  2019")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Если для ComVisible установить значение false, типы в этой сборке не будут поддерживаться 
// COM-компонентами.  При необходимости доступа к какому-либо типу в этой сборке 
// модели COM, задайте для атрибута ComVisible этого типа значение true.
[assembly: ComVisible(false)]

// Следующий GUID служит для идентификации библиотеки типов, если данный проект видим для COM
[assembly: Guid("dd1a3d1d-f030-4f5a-bdc1-7f024cbc1d30")]

// Сведения о версии сборки состоят из следующих четырех значений:
//
//      Основной номер версии
//      Дополнительный номер версии 
//   Номер сборки
//      Редакция
//
// Можно задать все значения или принять номера сборки и редакции по умолчанию 
// используя "*", как показано ниже:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
