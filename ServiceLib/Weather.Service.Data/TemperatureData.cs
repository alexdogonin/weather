﻿using System.Runtime.Serialization;

namespace Weather.Service.Data
{
    [DataContract]
    public class TemperatureData
    {
        [DataMember]
        public int Min { get; set; }
        [DataMember]
        public int Max { get; set; }

        public static implicit operator TemperatureData(Weather.Data.TemperatureData temperatureData)
        {
            return new TemperatureData()
            {
                Min = temperatureData.Min,
                Max = temperatureData.Max
            };
        }

        public static implicit operator Weather.Data.TemperatureData(TemperatureData temperatureData)
        {
            return new Weather.Data.TemperatureData(
                temperatureData.Min,
                temperatureData.Max
            );
        }
    }
}