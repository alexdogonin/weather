﻿using System;
using System.Runtime.Serialization;

namespace Weather.Service.Data
{
    /// <summary>
    /// Describes contract for service returning data 
    /// </summary>
    [DataContract]
    public class CityData
    {
        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public TemperatureData Temperature { get; set; }

        [DataMember]
        public DateTime OnDate { get; set; }

        public static implicit operator CityData(Weather.Data.CityData cityData)
        {
            return new CityData()
            {
                Name = cityData.Name,
                Temperature = cityData.Temperature,
                OnDate = cityData.OnDate
            };
        }

        public static implicit operator Weather.Data.CityData(CityData cityData)
        {
            return new Weather.Data.CityData(
                    cityData.Name,
                    cityData.Temperature,
                    cityData.OnDate
                );
        }
    }
}
